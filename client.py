# coding: utf-8

import socket
import time

HOST = '0.0.0.0'
PORT = 9000

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

for i in range(10):
    time.sleep(1)
    s.sendall('Hello, world' + str(i))
    data = s.recv(1024)
    time.sleep(1)
    print 'Received', repr(data)

s.close()
