# coding: utf-8

import uuid
import time
import socket
import signal
import threading
import multiprocessing


class Server(object):
    u"""TCP сервер."""

    def __init__(self, host, port, worker_num):

        self._worker_num = worker_num

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.bind((host, port))

        self._socket.listen(worker_num)

        self._stop_executing = False

        self._handler = None

        signal.signal(signal.SIGINT, self.stop)

    def stop(self, signam, frame):
        print 'Stopping'
        self._stop_executing = True

    @property
    def handler(self):
        return self._handler

    @handler.setter
    def handler(self, h):
        self._handler = h

    def run_forever(self):

        if self.handler is None:
            raise ValueError("Handler not specified.")

        workers = []
        while not self._stop_executing:
            client, _ = self._socket.accept()

            worker = self.handler(client.fileno())
            workers.append(worker)
            worker.start()

        self._socket.close()

        for worker in workers:
            worker.join()


class BaseEchoRequestHandler(object):
    u"""Обработчик запроса."""

    def __init__(self, client_socket, *args, **kwargs):
        super(BaseEchoRequestHandler, self).__init__(*args, **kwargs)
        self._client = socket.fromfd(
            client_socket, socket.AF_INET, socket.SOCK_STREAM)
        self._id = str(uuid.uuid4())

    def run(self):

        # Дать возможность главному потоку принять дополнительные запросы.
        time.sleep(0.1)

        while True:
            data = self._client.recv(1024)

            if not data:
                self._client.close()
                break

            if data == 'close':
                self._client.close()
                break
            else:
                self._client.sendall(data)


ThreadRequestHandler = type(
    'ThreadRequestHandler',
    (BaseEchoRequestHandler, threading.Thread),
    {}
)

ProcessRequestHandler = type(
    'ProcessRequestHandler',
    (BaseEchoRequestHandler, multiprocessing.Process),
    {}
)
